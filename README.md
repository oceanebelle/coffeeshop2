# Echo Service

Java code skeleton incorporating the following technologies

1. MAVEN
2. SPRING, SPRING MVC
3. JETTY
4. JSP

### Objective
Create a standalone java application (jar)
that exposes a RESTful web service (Echo).

### Solution
Embed Jetty within the application
and use Spring MVC framework to expose the Echo web service.
Echo web service uses JSP as view.
All resources are found within the jar application

### Walkthrough
The project using maven is made up of a top-level parent and
two child modules: **app** and **service**.

> The motivation for the split module design is to introduce a separation between
the application project and the service implementation ([Single responsibility principle](http://en.wikipedia.org/wiki/Single_responsibility_principle)) for future reuse.
However, at this point the only real advantage is the proof of concept
for the separation of code between two modules and using spring to wire all services together via import.

### app module
The **app** module contains startup code as well as wiring up an embedded jetty server.
The main class is defined in the *AppStart* class

    com.oceanebelle.app.AppStart

The *AppStart* class, loads up the spring context **app-context.xml** from the classpath.

The application utilises java annotations for defining the beans and dependency injections.
From the **app-context.xml**, the following code looks for annotated *@Configuration* classes
in the declared package. This is the main package used by the **app** module.

    <context:component-scan base-package="com.oceanebelle.app" />

The *AppConfiguration* class contains the bean definitions. The beans are identified
by the methods which are annotated with *@Bean*. Similarly, values injected from property files
are annotated with *@Value*

    @Configuration
    public class AppConfiguration {
        /**
         * Injected Properties
         */
        @Value("${web.service.port:8080}") int port;

        @Bean()
        public AppStart appStart(){
            return new AppStart();
        }

        @Bean
        public Server server() {
            Server server = new Server();
            return server;
        }

The following bean method factory is used to setup the embedded jetty server
with spring MVC. Note that this is found in AppConfiguration class.

    @Bean
    @DependsOn("server")
    public WebAppContext webAppContext() throws IOException, URISyntaxException {
        WebAppContext contextHandler = new WebAppContext();

        // import in the spring-service-contextHandler file all the spring mvc services.
        contextHandler.setInitParameter("contextConfigLocation", "classpath:spring-service-contextHandler.xml");

        AnnotationConfigWebApplicationContext springContext = new AnnotationConfigWebApplicationContext();

        // MVC SPRING dispatcherServlet
        DispatcherServlet dispatcherServlet = new DispatcherServlet(springContext);
        ServletHolder springServletHolder = new ServletHolder("dispatcherServlet",  dispatcherServlet);
        // Start the servlet on startup
        springServletHolder.setInitOrder(1);

        // Add to jetty's contexthandler
        contextHandler.addServlet(springServletHolder, "/");

        // Add jsp or default handler
        ServletHolder defaultServlet = new ServletHolder("default", new JspServlet());
        defaultServlet.setInitOrder(Integer.MAX_VALUE);
        contextHandler.addServlet(defaultServlet, "*.*");

        // Set the contextHandler to the handler
        ClassPathResource resource = new ClassPathResource("webapp");
        Log.info("RESOURCE_BASE={}", resource);
        String path = resource.getURI().toString();
        Log.info("RESOURCE_PATH={}", path);

        contextHandler.setContextPath("/");
        contextHandler.setResourceBase(path);

        contextHandler.addEventListener(new ContextLoaderListener());

        server().setHandler(contextHandler);

        return contextHandler;
    }

Key notes for the above jetty configuration:

- Use a spring xml definition for loading spring beans and MVC beans: *spring-service-contextHandler.xml*
- All jetty resources are found in the *webapp* directory (See app pom.xml).
- Define the *DispatcherServlet* which uses *AnnotationConfigWebApplicationContext*
- This DispatcherServlet is the entry point for spring MVC handlers. This servlet needs to
  start as soon as the server starts.
- Define a default servlet. Here we are using *JspServlet* class for serving jsp resources.
  The default servlet needs to be second handler after DispatcherServlet in the handler chain.
  This is done by adding *DispatcherServlet* first, then default servlet second in the same
  WebAppContext (Jetty's specialised contexthandler). Take note of the name of the
  default servlet used. In this instance, **default** is used.
- Add spring's *ContextLoaderListener* as event handler so that when the servlet context starts,
  the spring context xml configuration is loaded. Requires setting the servlet parameter
  **contextConfigLocation** to the path of the spring xml configuration containing
  the bean definitions for spring MVC.
- Note that the file specified in the **contextConfigLocation** needs to be accessible from the
  jetty base resource. That is **spring-service-context.xml** is found in the root directory of webapp.

So far we have only talked about the various bean definitions, next let's see how it all gets wired up together.

Going back to *AppStart.main()*, you'll see the following code snippet:

    ApplicationContext appContext =
                 new ClassPathXmlApplicationContext("app-context.xml");

    appContext.getBean(AppStart.class).start();

The above code, wires up all beans defined in app-context.xml, after which
The AppStart bean is fetched and start() method is called. The following code demonstrates
the AppStart instance:

    public class AppStart {

        @Autowired
        Server server;

        public void start() {
            Log.info("Starting as standalone server.");

            try {
                server.start();
                server.join();
            } catch (Exception e) {
                Log.error(e.getMessage(), e);
            }
        }
    }

AppStart instance contains a reference to the Jetty server which is injected by Spring
automatically. Starting the jetty server involves calling start() and join().

### service module

Going back to the file *spring-service-context.xml* in the app module we can see something like:

    <import resource="classpath*:echo-service-context.xml" />

The echo-service-context.xml file is found in the service module to which the app module references via
the maven dependency:

    <dependency>
        <groupId>com.oceanebelle</groupId>
        <artifactId>service</artifactId>
        <version>${project.version}</version>
    </dependency>

This means that when the embedded jetty servlet starts, the beans defined in
**echo-service-context.xml** is loaded in the spring container.
The key line contained in this file is the following:

    <context:component-scan base-package="com.oceanebelle.echo" />

In the same way as the app-context.xml file is loaded, we also use annotated @Configuration java classes
for the bean definitions within the com.oceanebelle.echo package. The annotated class is
**EchoConfiguration**

    @EnableWebMvc
    @Configuration
    @ComponentScan(basePackageClasses = EchoConfiguration.class)
    public class EchoConfiguration extends WebMvcConfigurerAdapter {
        private final static Logger Log = LoggerFactory.getLogger(EchoConfiguration.class);

        @Bean
        public InternalResourceViewResolver getViewResolver() {
            Log.info("--> getViewResolver()");
            InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
            viewResolver.setPrefix("/WEB-INF/jsp/");
            viewResolver.setSuffix(".jsp");
            viewResolver.setViewClass(org.springframework.web.servlet.view.JstlView.class);
            return viewResolver;
        }

        @Override
        public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
            // We want all non spring resources to be handled by the default servlet
            // named default.
            configurer.enable("default");
        }
    }

Key notes in the EchoConfiguration class

- @EnableWebMvc annotation tells spring to use Spring MVC framework for wiring up beans
- @ComponentScan annotation tells spring to search for MVC Web Services annotated by @Controller
  in the same package as the specified class (ie com.oceanebelle.echo).
- WebMvcConfigurerAdapter tells spring to extend some of the MVC capabilities. Here we are telling
  Spring to configure the defaultServletHandler as the servlet named "default".
- InternalResourceViewResolver is the class used by Spring MVC for handling views. We are telling
  Spring to use JSP views and at the same time specify the location of the JSP files in relation to
  the server base resource location (remember webapp?).

The annotated bean above gets loaded by Spring, after which it continues looking for
annotated web services. We have defined *EchoService* as our MVC web service with the following
code snippet.

    @Controller
    @RequestMapping("/echo")
    public class EchoService {
        private final static Logger Log = LoggerFactory.getLogger(EchoService.class);

        @RequestMapping(value = "/{data}", method = RequestMethod.GET)
        public String showGroovy(@PathVariable("data") String data, Model model) {
            model.addAttribute("data", data);
            return "echo";
        }
    }

Here, we have one resource which will be accessible via:

    http://<server>:<port>/echo/Any-Echo-Text

