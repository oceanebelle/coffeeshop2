package com.oceanebelle.app;

import org.apache.jasper.servlet.JspServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.ContextLoaderListener;

import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import java.io.IOException;
import java.net.URISyntaxException;


/**
 * Created by oceanebelle on 18/04/14.
 */
@Configuration
public class AppConfiguration {
    public final static Logger Log = LoggerFactory.getLogger(AppConfiguration.class);

    /**
     * Injected Properties
     */
    @Value("${web.service.port:8080}") int port;
    @Value("${web.service.accept.queueSize:5}") int acceptQueueSize;
    @Value("${web.service.host:localhost}") String host;

    @Bean()
    public AppStart appStart(){
        return new AppStart();
    }

    @Bean
    public Server server() {
        Server server = new Server();
        return server;
    }

    @Bean
    @DependsOn("server")
    public ServerConnector serverConnector() {
        ServerConnector connector = new ServerConnector(server());
        connector.setAcceptQueueSize(acceptQueueSize);
        connector.setHost(host);
        connector.setPort(port);
        server().addConnector(connector);
        return connector;
    }

    @Bean
    @DependsOn("server")
    public WebAppContext webAppContext() throws IOException, URISyntaxException {
        WebAppContext contextHandler = new WebAppContext();

        // import in the spring-service-contextHandler file all the spring mvc services.
        contextHandler.setInitParameter("contextConfigLocation", "classpath:spring-service-contextHandler.xml");

        AnnotationConfigWebApplicationContext springContext = new AnnotationConfigWebApplicationContext();

        // MVC SPRING dispatcherServlet
        DispatcherServlet dispatcherServlet = new DispatcherServlet(springContext);
        ServletHolder springServletHolder = new ServletHolder("dispatcherServlet",  dispatcherServlet);
        // Start the servlet on startup
        springServletHolder.setInitOrder(1);

        // Add to jetty's contexthandler
        contextHandler.addServlet(springServletHolder, "/");

        // Add jsp or default handler
        ServletHolder defaultServlet = new ServletHolder("default", new JspServlet());
        defaultServlet.setInitOrder(Integer.MAX_VALUE);
        contextHandler.addServlet(defaultServlet, "*.*");

        // Set the contextHandler to the handler
        ClassPathResource resource = new ClassPathResource("webapp");
        Log.info("RESOURCE_BASE={}", resource);
        String path = resource.getURI().toString();
        Log.info("RESOURCE_PATH={}", path);

        contextHandler.setContextPath("/");
        contextHandler.setResourceBase(path);

        contextHandler.addEventListener(new ContextLoaderListener());

        server().setHandler(contextHandler);

        return contextHandler;
    }

}
