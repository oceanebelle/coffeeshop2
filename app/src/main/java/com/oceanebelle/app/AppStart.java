package com.oceanebelle.app;

import org.eclipse.jetty.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by oceanebelle on 18/04/14.
 */
public class AppStart {
    private final static Logger Log = LoggerFactory.getLogger(AppStart.class);

    @Autowired
    Server server;

    public static void main (String[] args) {
        // Wire up all beans
        ApplicationContext appContext =
                 new ClassPathXmlApplicationContext("app-context.xml");

        appContext.getBean(AppStart.class).start();
    }

    /**
     * Automatically invoke once AppStart has been invoked.
     */
    public void start() {
        Log.info("Starting as standalone server.");

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            Log.error(e.getMessage(), e);
        }
    }
}
