package com.oceanebelle.echo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by oceanebelle on 18/04/14.
 */
@Controller
@RequestMapping("/echo")
public class EchoService {
    private final static Logger Log = LoggerFactory.getLogger(EchoService.class);

    @RequestMapping(value = "/{data}", method = RequestMethod.GET)
    public String showEcho(@PathVariable("data") String data, Model model) {
        model.addAttribute("data", data);
        return "echo";
    }
}
