package com.oceanebelle.echo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


/**
 * Created by oceanebelle on 18/04/14.
 */
@EnableWebMvc
@Configuration
@ComponentScan(basePackageClasses = EchoConfiguration.class)
public class EchoConfiguration extends WebMvcConfigurerAdapter {
    private final static Logger Log = LoggerFactory.getLogger(EchoConfiguration.class);

    @Bean
    public InternalResourceViewResolver getViewResolver() {
        Log.info("--> getViewResolver()");
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");
        viewResolver.setViewClass(org.springframework.web.servlet.view.JstlView.class);
        return viewResolver;
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        // We want all non spring resources to be handled by the default servlet
        // named default.
        configurer.enable("default");
    }
}
