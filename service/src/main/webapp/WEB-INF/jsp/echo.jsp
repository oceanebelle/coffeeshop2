<%@ include file="include.jsp" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Spring MVC Bootstrap</title>
</head>
<body>
    <div align="center">
        <h3>Welcome to Spring MVC Bootstrap demo!</h1>
        <h3>100% code-based approach, no XML at all! <%= new Date().toString()%></h3>
        <h1>${data}</h4>
    </div>
</body>
</html>